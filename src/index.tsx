import React from 'react';
import ReactDOM from 'react-dom/client';
import { BrowserRouter, Routes, Route } from "react-router-dom";
import './index.css';
import App from './App';
import reportWebVitals from './reportWebVitals';
import Parlay from './parlay/views/Parlay';
import Input from './parlay/views/Input';

const allUpdates = [
  '2023 nov 14 - last couple nights on white cushin chair',
  '2023 nov 10 - flying back to SEA from SF',
  '08/13/23'
];
const latestUpdate = allUpdates[0];
console.log('hello world\n-sahir (' + latestUpdate + ')');

const root = ReactDOM.createRoot(
  document.getElementById('root') as HTMLElement
);
root.render(
  // <React.StrictMode>
    <BrowserRouter>
      <Routes>
        <Route index element={<App />} />
        <Route path="/bet" element={<Parlay />} />
        <Route path="/input" element={<Input />} />
        <Route path="*" element={<App />} />
      </Routes>
    </BrowserRouter>
    // {/* <App /> */}
  // </React.StrictMode>
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
