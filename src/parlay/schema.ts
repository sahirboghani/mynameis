export type NBAGameSummary = {
    game: NBAGame,
    pregameLine: NBATotalEntry,
    liveLines: NBATotalEntry[],
    final: NBATotalEntry // use final.situation to differentiate regulation vs OT
}

export type NBAGame = {
    home: NBATeam,
    away: NBATeam,
    date: string, // bad idea? lets just always use YYYY-MM-DD. famous last words -nov 2023
    // pregameLines: NBATotalEntry
    // gameTotal: TotalWithOptionalLine,
    // homeTotal?: TotalWithOptionalLine,
    // awayTotal?: TotalWithOptionalLine
}

export type NBATotalEntry = {
    situation: NBASituation,
    gameTotal?: TotalWithOptionalLine,
    homeTotal?: TotalWithOptionalLine,
    awayTotal?: TotalWithOptionalLine,
    homeMoneyline: number,
    awayMoneyline: number
}

export type NBASituation = {
    time: NBATime,
    homePoints: number,
    awayPoints: number
}

export type NBATime = {
    period: NBAPeriod,
    secondsRemaining: number
}

export type TotalWithOptionalLine = {
    total: number,
    line?: number
}

export enum Side {
    UNSET,
    HOME,
    AWAY
}

export enum NBATeam {
    UNSET = 'UNSET',
    ATL = 'ATL',
    BOS = 'BOS',
    DET = 'DET',
    PHI = 'PHI',
    IND = 'IND',
    HOU = 'HOU',
    LAC = 'LAC',
    LAL = 'LAL',
    MIL = 'MIL',
    UTA = 'UTA',
    WAS = 'WAS'
}

export const NBATeamNames = Object.values(NBATeam).filter(team => team !== NBATeam.UNSET);

export enum NBAPeriod {
    UNSET,
    Q1,
    Q2,
    Q3,
    Q4,
    OT1,
    OT2,
    OT3,
    OT4,
    OT5
}


// const hardData: NBAGameSummary = {
//     game: {
//         home: NBATeam.PHI,
//         away: NBATeam.BOS,
//         date: '2023-11-08'
//         // gameTotal: {
//         //     total: 226.5,
//         //     line: -115
//         // }
//     },
//     pregameLine: 
//         {
//             situation: {
//                 time: {
//                     period: NBAPeriod.Q1,
//                     secondsRemaining: 12 * 60,
//                 },
//                 homePoints: 0,
//                 awayPoints: 0
//             },       
//             homeMoneyline: -115,
//             awayMoneyline: -115     
//         },
//     liveLines: [
//         {
//             situation: {
//                 time: {
//                     period: NBAPeriod.Q2,
//                     secondsRemaining: 0
//                 },
//                 homePoints: 61,
//                 awayPoints: 54
//             },
//             gameTotal: {
//                 total: 226,
//                 line: -115
//             },
//             homeMoneyline: -200,
//             awayMoneyline: 150
//         }
//     ],
//     final: {
//         situation: {
//             time: {
//                 period: NBAPeriod.Q4,
//                 secondsRemaining: 0
//             },
//             homePoints: 106,
//             awayPoints: 103
//         },
//         gameTotal: {
//             total: 209
//         },
//         homeTotal: {
//             total: 106
//         },
//         awayTotal: {
//             total: 103
//         },
//         homeMoneyline: 0,
//         awayMoneyline: 0
//     }
// }
