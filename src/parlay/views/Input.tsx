import { useState } from 'react';
import 'react-dropdown/style.css';

import { NBAGame, NBATeam, NBATeamNames, NBATotalEntry } from '../schema';

type Row = {
    id: number,
    game: NBAGame,
    entry: NBATotalEntry
}

function Input() {
    document.getElementById('sahirContent')?.style.setProperty('display', 'none');

    return (
        <NbaInput/>            
    )
}

function NbaInput() {
    const [ teamId, setTeamId ] = useState(''); 
    setTeamId(NBATeam.ATL)

    // const options = NBATeamNames.map(team => {
    //     const item = document.createElement('option');
    //     item.value = team;
    //     item.innerText = team;
    // })

    return (
        <div>
            {/* <label htmlFor={teamId}>
                Home: &nbsp;
            </label> */}
            <select id={teamId} name="selectedTeam">
                {
                    NBATeamNames.map(team => {
                        return (
                            <option value={team}>{team + ' - home'}</option>
                        );
                    })
                }
            </select>
            &nbsp; &nbsp;
            {/* <label htmlFor={teamId}>
            &nbsp; Away: &nbsp;
            </label> */}
            <select id={teamId} name="selectedTeam">
                {
                    NBATeamNames.map(team => {
                        return (
                            <option value={team}>{team + ' - away'}</option>
                        );
                    })
                }
            </select>
        </div>
    );
}

export default Input;