import { NBATeam, NBAPeriod, NBATotalEntry, NBAGame } from "../schema";
import DataTable from 'react-data-table-component';

type Row = {
    id: number,
    game: NBAGame,
    entry: NBATotalEntry
}

// const newWindow = window.open("https://www.betonline.ag/sportsbook/live-betting", "preview",'height=1000, width=1000');

function Parlay() {
    document.getElementById('sahirContent')?.style.setProperty('display', 'none');

    const home = (row: Row) => NBATeam[row.game.home];
    const away = (row: Row) => NBATeam[row.game.away];

    const columns = [
        {
            name: 'Current Score',
            selector: (row: Row) => `(${home(row)}) ${row.entry.homeTotal?.total} - (${away(row)}) ${row.entry.awayTotal?.total}`
        },
        {
            name: 'Home Total',
            selector: (row: Row) => `(${home(row)}) Pregame ${row.entry.homeTotal?.total} Now ${row.entry.homeTotal?.total} (${row.entry.homeTotal?.line})`
        },
        {
            name: 'Home Moneyline',
            selector: (row: Row) => `(${home(row)}) Pregame ${row.entry.homeMoneyline} Now ${row.entry.homeMoneyline}`
        },
    ];
    
    const data = [
        {
            id: 1,
            game: {
                home: NBATeam.MIL,
                away: NBATeam.BOS,
                date: 'now'
            },
            entry: {
                situation: {
                    time: {
                        period: NBAPeriod.Q1,
                        secondsRemaining: 12 * 60
                    },
                    homePoints: 0,
                    awayPoints: 0
                },
                homeMoneyline: 115,
                awayMoneyline: 115
            },
        }
    ]
    
    // const iframe = (
    //     <Iframe url="https://www.betonline.ag/sportsbook/live-betting"
    //             width="500px"
    //             height="500px"
    //             id="betonline"
    //             className=""
    //             display="block"
    //             position="relative"
    //         />
    // );
    
    // let iterations = 0;
    // const intervalId = setInterval(() => {
    //     if (iterations === 10) {
    //         clearInterval(intervalId);
    //     }
    //     ++iterations;

    //     const win = newWindow;

    //     // debugger
    // }, 5000);

    return (
        <div>
            <DataTable
                columns={columns}
                data={data}
            />
            {/* {iframe} */}
        </div>
    );
}

// const hardData: NBAGameSummary = {
//     game: {
        
//     },
//     liveEntries: [
//         {
//             situation: {
//                 period: NBAPeriod.Q2,
//                 secondsRemaining: 0,
//                 homePoints: 61,
//                 awayPoints: 54
//             },
//             gameTotal: {
//                 total: 226,
//                 line: -115
//             }
//         }
//     ],
//     final: {
//         situation: {
//             period: NBAPeriod.Q4,
//             secondsRemaining: 0,
//             homePoints: 106,
//             awayPoints: 103
//         },
//         gameTotal: {
//             total: 209
//         },
//         homeTotal: {
//             total: 106
//         },
//         awayTotal: {
//             total: 103
//         }
//     }
// }

export default Parlay;